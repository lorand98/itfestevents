package com.technoloid.itfestevents;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by validraganescu on 06/04/2017.
 */

public class EventAdapter extends BaseAdapter {

    private List<EventItem> events;
    private Context context;

    public EventAdapter(Context context, List<EventItem> events) {
        this.events = events;
        this.context = context;
    }

    @Override
    public int getCount() {
        return events.size();
    }

    @Override
    public Object getItem(int position) {
        return events.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

//        if (convertView == null) {
//            convertView = View.inflate(context, R.layout.event_item, null);
//        }
        if (position % 2 == 0) {
            convertView = View.inflate(context, R.layout.event_item, null);
        } else {
            convertView = View.inflate(context, R.layout.event_item_alternate, null);
        }

        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView description = (TextView) convertView.findViewById(R.id.description);
        TextView date = (TextView) convertView.findViewById(R.id.date);

        EventItem eventItem = events.get(position);

        title.setText(eventItem.getEventName());
        description.setText(eventItem.getEventDescription());
        date.setText(eventItem.getEventDate());

        return convertView;
    }
}
